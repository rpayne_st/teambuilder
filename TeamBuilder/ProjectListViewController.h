//
//  ProjectListViewController.h
//  TeamBuilder
//
//  Created by PGM on 2/12/15.
//  Copyright (c) 2015 PGM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *projectTableView;

@end
