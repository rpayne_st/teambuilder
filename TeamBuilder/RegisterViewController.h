//
//  RegisterViewController.h
//  TeamBuilder
//
//  Created by PGM on 2/12/15.
//  Copyright (c) 2015 PGM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *buttonEngineer;
@property (weak, nonatomic) IBOutlet UIButton *buttonDesigner;
@property (weak, nonatomic) IBOutlet UIButton *buttonMarketer;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *passwordConfirmField;
- (IBAction)buttonEngineer_Click:(id)sender;
- (IBAction)buttonDesigner_Click:(id)sender;
- (IBAction)buttonMarketer_Click:(id)sender;
- (IBAction)registerNow:(id)sender;




@end
