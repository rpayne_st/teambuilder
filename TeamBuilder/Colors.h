//
//  Colors.h
//  Transitions
//
//  Created by Daniel.Burke on 3/12/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import <Foundation/Foundation.h>
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define LIGHT_ORANGE UIColorFromRGB(0xFFB354)
#define ORANGE UIColorFromRGB(0xFF9613)
#define DARK_ORANGE UIColorFromRGB(0xB26607)
#define BLUE UIColorFromRGB(0x259AF7)
#define DARK_BLUE UIColorFromRGB(0x0077B2)

#define LIGHT_BLUE UIColorFromRGB(0x259AF7)
#define WHITE [UIColor whiteColor]
#define LIGHT_GRAY UIColorFromRGB(0xCCCCCC)
#define GRAY UIColorFromRGB(0x666666)
#define GREEN UIColorFromRGB(0x339900)
#define CLEAR [UIColor clearColor];


#define FACEBOOK_BLUE UIColorFromRGB(0x4667AC)
#define TWITTER_BLUE UIColorFromRGB(0x22B2EF)
#define GOOGLE_RED UIColorFromRGB(0xDA4936)
#define PINTEREST_RED UIColorFromRGB(0xCB2028)
#define EMAIL_BLUE UIColorFromRGB(0x46b5ef)
#define MESSAGES_GREEN UIColorFromRGB(0x4fec43)

@interface Colors : NSObject

@end
