//
//  RegisterViewController.m
//  TeamBuilder
//
//  Created by PGM on 2/12/15.
//  Copyright (c) 2015 PGM. All rights reserved.
//

#import "RegisterViewController.h"
#import "Colors.h"


@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.buttonDesigner.layer.cornerRadius = 50.f;
    self.buttonDesigner.layer.borderWidth = 3.f;
    self.buttonDesigner.layer.borderColor = UIColorFromRGB(0xAD1F15).CGColor;
    self.buttonEngineer.layer.cornerRadius = 50.f;
    self.buttonEngineer.layer.borderWidth = 3.f;
    self.buttonEngineer.layer.borderColor = UIColorFromRGB(0xE4B517).CGColor;
    self.buttonMarketer.layer.cornerRadius = 50.f;
    self.buttonMarketer.layer.borderWidth = 3.f;
    self.buttonMarketer.layer.borderColor = UIColorFromRGB(0x375216).CGColor;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)buttonEngineer_Click:(id)sender {
    
    
    
}

- (IBAction)buttonDesigner_Click:(id)sender {
    
    
    
}

- (IBAction)buttonMarketer_Click:(id)sender {
    
    
    
}

- (IBAction)registerNow:(id)sender {
}
@end
